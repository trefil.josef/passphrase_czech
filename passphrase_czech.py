import unicodedata
from itertools import chain
from random import randint
from secrets import choice
from typing import List

from word_list import word_list


class PassphraseCzech:
    WORD_LIST = word_list

    @classmethod
    def generate_passphrase(cls, words: int, caps: int, digits: int, spec: int, diacritic: bool) -> str:
        symbols, unchanged_positions = cls._get_just_words(words, diacritic)
        symbols, unchanged_positions = cls._insert(symbols, unchanged_positions, to_insert='caps', count=caps)
        symbols, unchanged_positions = cls._insert(symbols, unchanged_positions, to_insert='digits', count=digits)
        symbols, unchanged_positions = cls._insert(symbols, unchanged_positions, to_insert='spec', count=spec)
        return ''.join(symbols)

    @classmethod
    def _get_just_words(cls, words: int, diacritic: bool) -> (List[str], List[int]):
        passphrase = []
        for _ in range(words):
            while True:
                chosen = choice(cls.WORD_LIST)
                if not diacritic:
                    chosen = cls._strip_diacritic(chosen)
                if chosen not in passphrase:
                    passphrase.append(chosen)
                    break
        symbols = list(chain(*passphrase))
        unchanged_positions = list(range(len(symbols)))
        return symbols, unchanged_positions

    @classmethod
    def _insert(cls, symbols: List[str], unchanged_positions: List[int], to_insert: str, count: int) -> (
            List[str], List[int]):
        for _ in range(count):
            position = choice(unchanged_positions)
            if (symbols[position].islower()) and (not symbols[position].isdigit()):
                insert_method = getattr(cls, f'_{to_insert}')
                symbols[position] = insert_method(symbols[position])
            unchanged_positions.remove(position)
        return symbols, unchanged_positions

    @staticmethod
    def _caps(symbol: str) -> str:
        return symbol.upper()

    @staticmethod
    def _digits(symbol: str) -> str:
        return str(randint(0, 9))

    @staticmethod
    def _spec(symbol: str) -> str:
        return choice(
            ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '=', '{', '}', '[', ']', '|', '\\', '/',
             ':', ';', '"', '\'', '<', '>', ',', '.', '?'])

    @staticmethod
    def _strip_diacritic(word: str) -> str:
        return ''.join(c for c in unicodedata.normalize('NFD', word)
                       if unicodedata.category(c) != 'Mn')